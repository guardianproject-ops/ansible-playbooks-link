---
# WARNING
# this file is automaticaly generated. your changes will not be saved.
# WARNING
plugin: amazon.aws.aws_ec2
regions:
  - {{getenv "AWS_REGION"}}
hostnames:
  - tag:Name
  - instance-id
filters:
  tag:Namespace: {{getenv "NAMESPACE"}}
  tag:Environment: {{getenv "ENVIRONMENT"}}
compose:
  ansible_host: instance_id
groups:
  aws: true
keyed_groups:
  # Create a group for each value of the Application tag
  - key: tags.Application
    separator: ''
  - key: tags.Attributes
    separator: ''
