-include ../Makefile.env
ROLES_DIR ?= roles
COLLECTIONS_DIR ?= collections
GALAXY_ARGS ?=
INVENTORY_OUT ?= aws_ec2.yml
WATERBEAR_ANSIBLE_PATH ?= .

## Download all ansible roles and collection dependencies
deps:
	ansible-galaxy role install -r requirements.yml -p $(ROLES_DIR) $(GALAXY_ARGS)
	ansible-galaxy collection install -r requirements.yml -p $(COLLECTIONS_DIR) $(GALAXY_ARGS)

deps-force:
	ansible-galaxy role install -r requirements.yml -p $(ROLES_DIR) --force $(GALAXY_ARGS)
	ansible-galaxy collection install -r requirements.yml -p $(COLLECTIONS_DIR) --force $(GALAXY_ARGS)

## Clean up all generated ansible artifacts
clean:
	@rm -rf $(COLLECTIONS_DIR) $(ROLES_DIR) ansible.zip
