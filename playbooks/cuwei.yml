---
- name: Get all hosts facts
  hosts: cuwei
  gather_facts: false
  become: true
  tags: [wait]
  pre_tasks:
    - name: wait for system to be available
      import_tasks: ../collections/ansible_collections/gpops/common/tasks/wait.yml

- name: Configure base system
  hosts: cuwei
  become: true
  vars_files:
    - vars/vector.yml
    - vars/cuwei.yml
  roles:
    - {role: ansible-firewall-docker, tags: [firewall]}
    - {role: ansible-docker-host, tags: [docker]}
    - {role: ansible-cloudflared, tags: [cloudflared]}
    - {role: ansible-vector, tags: [vector]}

- name: Configure cuwei
  hosts: cuwei
  become: true
  tags: [cuwei]
  vars_files:
    - vars/cuwei.yml
  pre_tasks:
    - name: motd
      import_tasks: ../collections/ansible_collections/gpops/common/tasks/motd.yml
      tags:
        - configure

  tasks:
    - name: Ensure directories
      file:
        path: "{{ item }}"
        owner: root
        group: root
        state: directory
      with_items:
        - /var/lib/cuwei-docker-compose
        - /var/lib/cdr
        - /var/lib/nginx

    - name: Install nginx conf
      template:
        src: cuwei-nginx.conf
        dest: /var/lib/nginx/cuwei.conf
        owner: root
        group: root

    - name: database
      import_tasks: cuwei-database.yml

    - name: Update the compose definitions
      template:
        src: cuwei-docker-compose.yml
        dest: /var/lib/cuwei-docker-compose/docker-compose.yml
        owner: root
        group: root

    - name: Log into private docker image registry
      docker_login:
        registry: "{{ docker_registry }}"
        username: "{{ docker_registry_username }}"
        password: "{{ docker_registry_password }}"
        reauthorize: true
      when: docker_registry is defined

    - name: Run docker-compose to bring up the containers
      docker_compose:
        project_src: /var/lib/cuwei-docker-compose
        project_name: cuwei
        state: present
        stopped: false
        pull: true
        services:
          - cuwei-backend
          - cuwei-worker
          - cuwei-frontend
          - nginx
      register: output

    - name: Check that all containers are running
      assert:
        that:
          - 'output.ansible_facts["cuwei-backend"]["cuwei_cuwei-backend_1"].state.running'
          - 'output.ansible_facts["cuwei-worker"]["cuwei_cuwei-worker_1"].state.running'
          - 'output.ansible_facts["cuwei-frontend"]["cuwei_cuwei-frontend_1"].state.running'
          - 'output.ansible_facts["nginx"]["cuwei_nginx_1"].state.running'

  post_tasks:
    - name: Ensure vector can read logfiles
      user:
        name: vector
        groups: adm
        append: true
      tags: vector
      notify:
        - restart vector
    - name: Ensure proper permissions on vector data dir
      file:
        path: /var/lib/vector
        state: directory
        owner: vector
        group: vector
        recurse: true
      tags: vector

    - name: Post Install
      debug:
        msg:
          - "Server Launched: {{ domain }}"
