---
- include_vars: ../vars/cuwei-rds.yml

- name: Install pg_catalog schema extension
  postgresql_ext:
    name: plpgsql
    state: present
    cascade: true
    db: "{{ database_name }}"
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username }}"
    login_password: "{{ rds_admin_password }}"

- name: Install public schema extensions
  postgresql_ext:
    name: "{{ item }}"
    state: present
    cascade: true
    db: "{{ database_name }}"
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username }}"
    login_password: "{{ rds_admin_password }}"
  with_items:
    - uuid-ossp
    - citext
    - pgcrypto
    - tablefunc

- name: Create authenticator db user
  postgresql_user:
    db: "{{ database_name }}"
    name: "{{ database_authenticator_username }}"
    password: "{{ database_authenticator_password }}"
    priv: "CONNECT"
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username}}"
    login_password: "{{ rds_admin_password }}"
  no_log: true

- name: Create visitor db user
  postgresql_user:
    db: "{{ database_name }}"
    name: "{{ database_visitor_username }}"
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username }}"
    login_password: "{{ rds_admin_password }}"
  no_log: true

- name: Create anonymous db user
  postgresql_user:
    db: "{{ database_name }}"
    name: "{{ database_anonymous_username }}"
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username }}"
    login_password: "{{ rds_admin_password }}"
  no_log: true

- name: Create non-admin db users
  postgresql_user:
    db: "{{ database_name }}"
    name: "{{ item }}"
    groups:
      - "{{ database_anonymous_username }}"
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username }}"
    login_password: "{{ rds_admin_password }}"
  no_log: true
  with_items:
    - "{{ database_analyst_username }}"
    - "{{ database_investigator_username }}"

- name: Create senior analyst user
  postgresql_user:
    db: "{{ database_name }}"
    name: "{{ database_senior_analyst_username }}"
    groups:
      - "{{ database_analyst_username }}"
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username }}"
    login_password: "{{ rds_admin_password }}"
  no_log: true

- name: Create admin db user
  postgresql_user:
    db: "{{ database_name }}"
    name: "{{ database_admin_username }}"
    groups:
      - "{{ database_senior_analyst_username }}"
      - "{{ database_investigator_username }}"
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username}}"
    login_password: "{{ rds_admin_password }}"
  no_log: true

- name: Run queries
  postgresql_query:
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username }}"
    login_password: "{{ rds_admin_password }}"
    db: "{{ database_name }}"
    query: "{{ item }}"
  with_items:
    - "GRANT {{ database_visitor_username }} TO {{ database_authenticator_username }}"
    - "REVOKE ALL ON DATABASE {{ database_name }} FROM PUBLIC"
    - "GRANT wb_anonymous TO {{ database_visitor_username }}"
    - "GRANT wb_analyst TO {{ database_visitor_username }}"
    - "GRANT wb_senioranalyst TO {{ database_visitor_username }}"
    - "GRANT wb_investigator TO {{ database_visitor_username }}"
    - "GRANT wb_admin TO {{ database_visitor_username }}"
