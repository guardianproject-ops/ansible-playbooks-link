---
- include_vars: ../vars/metamigo-rds.yml

- name: Install pg_catalog schema extension
  postgresql_ext:
    name: plpgsql
    state: present
    cascade: true
    db: "{{ database_name }}"
    schema: "pg_catalog"
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username }}"
    login_password: "{{ rds_admin_password }}"

- name: Install public schema extensions
  postgresql_ext:
    name: "{{ item }}"
    state: present
    cascade: true
    db: "{{ database_name }}"
    schema: "public"
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username }}"
    login_password: "{{ rds_admin_password }}"
  with_items:
    - uuid-ossp
    - citext
    - pgcrypto
    - tablefunc

- name: Create authenticator db user
  postgresql_user:
    db: "{{ database_name }}"
    name: "app_graphile_auth"
    password: "{{ database_authenticator_password }}"
    priv: "CONNECT"
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username}}"
    login_password: "{{ rds_admin_password }}"
  no_log: true

- name: Create visitor db user
  postgresql_user:
    db: "{{ database_name }}"
    name: "app_user"
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username }}"
    login_password: "{{ rds_admin_password }}"
  no_log: true

- name: Create admin db user
  postgresql_user:
    db: "{{ database_name }}"
    name: "app_admin"
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username }}"
    login_password: "{{ rds_admin_password }}"
  no_log: true

- name: Create anonymous db user
  postgresql_user:
    db: "{{ database_name }}"
    name: "app_anonymous"
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username }}"
    login_password: "{{ rds_admin_password }}"
  no_log: true

- name: Run queries
  postgresql_query:
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username }}"
    login_password: "{{ rds_admin_password }}"
    db: "{{ database_name }}"
    query: "{{ item }}"
  with_items:
    - "REVOKE ALL ON DATABASE {{ database_name }} FROM PUBLIC"
    - "CREATE ROLE app_anonymous"
    - "CREATE ROLE app_user WITH IN ROLE app_anonymous"
    - "CREATE ROLE app_admin WITH IN ROLE app_user"
    - "GRANT app_anonymous TO app_user"
    - "GRANT app_anonymous TO app_graphile_auth"
    - "GRANT app_admin TO app_graphile_auth"
    - "GRANT app_user TO app_graphile_auth"
    - "GRANT ALL PRIVILEGES ON DATABASE {{ database_name }} TO metamigo"
    - "GRANT CONNECT ON DATABASE {{ database_name }} TO app_graphile_auth"
  ignore_errors: true

- name: Create users
  postgresql_query:
    login_host: "{{ rds_host }}"
    login_user: "{{ rds_admin_username }}"
    login_password: "{{ rds_admin_password }}"
    db: "{{ database_name }}"
    query: |
      INSERT INTO app_public.users (email, name, user_role, is_active, created_by)
      VALUES ('{{ item.email }}', '{{ item.name }}', 'admin'::app_public.role_type, true,
      'metamigo-users') ON CONFLICT (email) DO NOTHING
  with_items:
    - email: abel@gpcmdln.net
      name: abel
    - email: irl@sr2.uk
      name: irl
    - email: ana@sr2.uk
      name: ana
    - email: josh@digiresilience.org
      name: josh
    - email: darren@redaranj.com
      name: darren
