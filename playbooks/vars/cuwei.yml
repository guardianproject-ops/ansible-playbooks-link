---
docker_users:
  - vector
ssm_prefix: "{{ ansible_local.aws_tags.ssmprefix }}"
aws_region: "{{ ansible_local.aws_instance.region }}"
ssm_vars: "{{ lookup('aws_ssm', ssm_prefix, region=aws_region, shortnames=true, bypath=true, recursive=true) }}"
firewall_open_ports_tcp:
  - 9100 # nodeexporter metrics
  - 9300 # cloudflared metrics
  - 9395 # cuwei backend metrics
  - 9496 # cuwei worker metrics
  - 9597 # cuwei frontend metrics
cloudflare_account_id: "{{ ssm_vars.cloudflare_account_id }}"
cloudflare_tunnel_name: "{{ ssm_vars.cloudflare_tunnel_name }}"
cloudflare_tunnel_id: "{{ ssm_vars.cloudflare_tunnel_id }}"
cloudflare_tunnel_secret: "{{ ssm_vars.cloudflare_tunnel_secret }}"
cloudflared_tunnels:
  - name: "{{ cloudflare_tunnel_name }}"
    id: "{{ cloudflare_tunnel_id }}"
    secret: "{{ cloudflare_tunnel_secret }}"
    config:
      url: "http://localhost:80"
      logfile: /var/log/cloudflared/tunnel.log
      metrics: "0.0.0.0:9300"
vector_sources: "{{ docker_source | combine(journald_source) }}"
cuwei_transforms:
  cuwei_split:
    type: swimlanes
    inputs:
      - docker_redacted
    lanes:
      backend:
        container_name.regex: ".*cuwei-backend.*"
      frontend:
        container_name.regex: ".*cuwei-frontend.*"
vector_transforms: "{{ cuwei_transforms | combine(tagging_transform, redact_transform) }}"
cuwei_sink:
  cuwei-backend:
    type: aws_cloudwatch_logs
    inputs: [cuwei_split.backend]
    group_name: "{{ ssm_vars.log_group_cuwei_backend }}"
    region: "{{ aws_region }}"
    stream_name: "{{ ansible_local.aws_instance.instance_id }}"
    encoding:
      codec: json
  cuwei-frontend:
    type: aws_cloudwatch_logs
    inputs: [cuwei_split.frontend]
    group_name: "{{ ssm_vars.log_group_cuwei_frontend }}"
    region: "{{ aws_region }}"
    stream_name: "{{ ansible_local.aws_instance.instance_id }}"
    encoding:
      codec: json
vector_sinks: "{{ cuwei_sink | combine(journald_sink) }}"
service_motd: |-
  * Cuwei Backend:
  *   status  - docker ps cuwei_cuwei-backend_1
  *   restart - docker restart cuwei_cuwei-backend_1
  *   logs    - docker logs cuwei_cuwei-backend_1
  *
  * Cuwei Worker:
  *   status  - docker ps cuwei_cuwei-worker_1
  *   restart - docker restart cuwei_cuwei-worker_1
  *   logs    - docker logs cuwei_cuwei-worker_1
  *
  * Cuwei Frontend:
  *   status  - docker ps cuwei_cuwei-frontend_1
  *   restart - docker restart cuwei_cuwei-frontend_1
  *   logs    - docker logs cuwei_cuwei-frontend_1
  *
  * Nginx:
  *   status  - docker ps cuwei_nginx_1
  *   restart - docker restart cuwei_nginx_1
  *   logs    - docker logs cuwei_nginx_1
  *
  * Cloudflare Argo Tunnel:
  *   status  - systemctl status cloudflared@{{ cloudflare_tunnel_name }}.service
  *   restart - systemctl restart cloudflared@{{ cloudflare_tunnel_name }}.service
  *   logs    - tail -f -n 100 /var/log/cloudflared/tunnel.log
  *   config  - /etc/cloudflared

domain: "{{ ssm_vars.cuwei_domain }}"
backend_domain: "{{ ssm_vars.cuwei_backend_domain }}"
zammad_graphql_url: "{{ ssm_vars.zammad_graphql_url }}"
zammad_api_url: "{{ ssm_vars.zammad_api_url }}"
zammad_api_token: "{{ ssm_vars.zammad_api_token }}"
cloudflare_audience: "{{ ssm_vars.cloudflare_audience }}"
cloudflare_url: "{{ ssm_vars.cloudflare_url }}"
cloudflare_client_id: "{{ ssm_vars.cloudflare_client_id }}"
cloudflare_client_secret: "{{ ssm_vars.cloudflare_client_secret }}"
docker_registry: "{{ ssm_vars.docker_registry }}"
docker_registry_username: "{{ ssm_vars.docker_registry_username }}"
docker_registry_password: "{{ ssm_vars.docker_registry_password }}"
docker_frontend_image: "{{ ssm_vars.docker_frontend_image }}"
docker_backend_image: "{{ ssm_vars.docker_backend_image }}"
database_host: "{{ ssm_vars.database_host }}"
database_name: "{{ ssm_vars.database_name }}"
database_superuser_username: "{{ ssm_vars.database_superuser_username }}"
database_superuser_password: "{{ ssm_vars.database_superuser_password }}"
database_username: "{{ ssm_vars.database_username }}"
database_password: "{{ ssm_vars.database_password }}"
database_authenticator_username: "{{ ssm_vars.database_authenticator_username }}"
database_authenticator_password: "{{ ssm_vars.database_authenticator_password }}"
database_visitor_username: "{{ ssm_vars.database_visitor_username }}"
database_admin_username: "{{ ssm_vars.database_admin_username }}"
database_senior_analyst_username: "{{ ssm_vars.database_senior_analyst_username }}"
database_investigator_username: "{{ ssm_vars.database_investigator_username }}"
database_analyst_username: "{{ ssm_vars.database_analyst_username }}"
database_anonymous_username: "{{ ssm_vars.database_anonymous_username }}"
exports_bucket: "{{ ssm_vars.exports_bucket }}"
