---
docker_users:
  - vector
ssm_prefix: "{{ ansible_local.aws_tags.ssmprefix }}"
aws_region: "{{ ansible_local.aws_instance.region }}"
ssm_vars: "{{ lookup('aws_ssm', ssm_prefix, region=aws_region, shortnames=true, bypath=true, recursive=true) }}"
firewall_open_ports_tcp:
  - 9100 # nodeexporter metrics
  - 9300 # cloudflared metrics
  - 9395 # cunei backend metrics
  - 9496 # cunei worker metrics
cloudflare_account_id: "{{ ssm_vars.cloudflare_account_id }}"
cloudflare_tunnel_name: "{{ ssm_vars.cloudflare_tunnel_name }}"
cloudflare_tunnel_id: "{{ ssm_vars.cloudflare_tunnel_id }}"
cloudflare_tunnel_secret: "{{ ssm_vars.cloudflare_tunnel_secret }}"
cloudflared_tunnels:
  - name: "{{ cloudflare_tunnel_name }}"
    id: "{{ cloudflare_tunnel_id }}"
    secret: "{{ cloudflare_tunnel_secret }}"
    config:
      url: "http://localhost:3000"
      logfile: /var/log/cloudflared/tunnel.log
      metrics: "0.0.0.0:9300"
vector_sources: "{{ docker_source | combine(journald_source, cloudflared_source) }}"
cunei_transforms:
  cunei_split:
    type: swimlanes
    inputs:
      - docker_redacted
    lanes:
      backend:
        container_name.regex: ".*cunei-backend.*"
      worker:
        container_name.regex: ".*cunei-worker.*"
      redis:
        container_name.regex: ".*redis.*"
vector_transforms: "{{ cunei_transforms | combine(tagging_transform, redact_transform) }}"
cunei_sink:
  cunei-backend:
    type: aws_cloudwatch_logs
    inputs: [cunei_split.backend]
    group_name: "{{ ssm_vars.log_group_cunei_backend }}"
    region: "{{ aws_region }}"
    stream_name: "{{ ansible_local.aws_instance.instance_id }}"
    encoding:
      codec: json
  cunei-worker:
    type: aws_cloudwatch_logs
    inputs: [cunei_split.worker]
    group_name: "{{ ssm_vars.log_group_cunei_worker }}"
    region: "{{ aws_region }}"
    stream_name: "{{ ansible_local.aws_instance.instance_id }}"
    encoding:
      codec: json
  cunei-redis:
    type: aws_cloudwatch_logs
    inputs: [cunei_split.redis]
    group_name: "{{ ssm_vars.log_group_cunei_redis }}"
    region: "{{ aws_region }}"
    stream_name: "{{ ansible_local.aws_instance.instance_id }}"
    encoding:
      codec: json
vector_sinks: "{{ cunei_sink | combine(journald_sink, cloudflared_sink) }}"
service_motd: |-
  * Cunei Backend:
  *   status  - docker ps cunei_cunei-backend_1
  *   restart - docker restart cunei_cunei-backend_1
  *   logs    - docker logs cunei_cunei-backend_1
  *
  * Cunei Worker:
  *   status  - docker ps cunei_cunei-worker_1
  *   restart - docker restart cunei_cunei-worker_1
  *   logs    - docker logs cunei_cunei-worker_1
  *
  * Cunei Redis:
  *   status  - docker ps cunei_redis_1
  *   restart - docker restart cunei_redis_1
  *   logs    - docker logs cunei_redis_1
  *
  * Cloudflare Argo Tunnel:
  *   status  - systemctl status cloudflared@{{ cloudflare_tunnel_name }}.service
  *   restart - systemctl restart cloudflared@{{ cloudflare_tunnel_name }}.service
  *   logs    - tail -f -n 100 /var/log/cloudflared/tunnel.log
  *   config  - /etc/cloudflared
cunei_domain: "{{ ssm_vars.cunei_domain }}"
docker_registry_username: "{{ ssm_vars.docker_registry_username }}"
docker_registry_password: "{{ ssm_vars.docker_registry_password }}"
docker_registry: "{{ ssm_vars.docker_registry }}"
docker_image: "{{ ssm_vars.docker_image }}"
cunei_admin_username: "{{ ssm_vars.cunei_admin_username }}"
cunei_admin_password: "{{ ssm_vars.cunei_admin_password }}"
cloudflare_audience: "{{ ssm_vars.cloudflare_audience }}"
cloudflare_url: "{{ ssm_vars.cloudflare_url }}"
cloudflare_client_id: "{{ ssm_vars.cloudflare_client_id }}"
cloudflare_client_secret: "{{ ssm_vars.cloudflare_client_secret }}"
cunei_secrets: "{{ ssm_vars.cunei_secrets }}"
zammad_group: "{{ ssm_vars.zammad_group }}"
zammad_api_url: "{{ ssm_vars.zammad_api_url }}"
zammad_token: "{{ ssm_vars.zammad_token }}"
wayback_api_url: "{{ ssm_vars.wayback_api_url }}"
wayback_access_key: "{{ ssm_vars.wayback_access_key }}"
wayback_secret_key: "{{ ssm_vars.wayback_secret_key }}"
perma_api_url: "{{ ssm_vars.perma_api_url }}"
perma_api_key: "{{ ssm_vars.perma_api_key }}"
redis_data_dir: /var/lib/cdr/redis
